from django.db import models

class Company(models.Model):
    name = models.CharField(max_length=200, unique=True)
    address = models.CharField(max_length=200)
    rut = models.CharField(max_length=200, unique=True)
    phone = models.IntegerField()

    def __str__(self):
        return self.name

class Employee(models.Model):
    fullname = models.CharField(max_length=200, unique=True)
    rutEmployee = models.CharField(max_length=9, unique=True)
    email = models.CharField(max_length=200, unique=True)
    company_id = models.ForeignKey(Company, on_delete=models.CASCADE)

    def __str__(self):
        return self.fullname
    