from rest_framework import generics
from .models import Company, Employee
from .serializers import CompanySerializer, EmployeeSerializer

class EmployeeList(generics.ListCreateAPIView):
    serializer_class = EmployeeSerializer

    def get_queryset(self):
        queryset = Employee.objects.all()
        company = self.request.query_params.get('company')
        
        if company is not None:
            queryset = queryset.filter(company_id=company)
            return queryset
        
class EmployeeDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = EmployeeSerializer
    queryset = Employee.objects.all()
   
class CompanyList(generics.ListCreateAPIView):
    serializer_class = CompanySerializer
    queryset = Company.objects.all()

class CompanyDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = CompanySerializer
    queryset = Company.objects.all()
    