from django.urls import path
from .views import CompanyList, CompanyDetail, EmployeeList, EmployeeDetail

urlpatterns = [
   path('company/', CompanyList.as_view()),
   path('company/<int:pk>/', CompanyDetail.as_view()),
   path('employee/', EmployeeList.as_view()),
   path('employee/<int:pk>/', EmployeeDetail.as_view()),
]