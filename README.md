## Name
API

## Description
Crud application incomplete (can only list and create). This application is a backend requested for the selection process in SimpliLatam company. In this moment with SQLite

## Installation

For Windows User. (yo must to have installed Python/Pip)

1) git clone https://gitlab.com/gon_of_war/api.git

2) cd api

3) py -m pip install virtualenv

4) py -m virtualenv venv

5) .\venv\Scripts\activate.bat

6) pip3 install django djangorestframework

7) py -m pip install django-cors-headers

8) py manage.py createsuperuser

9) py manage.py makemigrations

10) py manage.py migrate

11) py manage.py runserver (you must be in the virtual env inside)

## Usage

- http://localhost:8000/api/company/ => list of companies (GET)

- http://localhost:8000/api/company/ => create a company (POST)

- http://localhost:8000/api/employee/ => create an employee, setting FK (company) (POST) 

## CORS

Currently CORS are implemented to receive requests from localhost:4200 (Angular's default port). If you want to use another source, you must modify it in project/settings.py 

CORS_ALLOWED_ORIGINS = [
    'http://localhost:4200'
]